from django.shortcuts import render, redirect
from lab_1.models import Friend
from lab_3.forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url = "/admin/login/")
def index(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url = "/admin/login/")
def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if request.method == "POST":

        if form.is_valid():
        # save the form data to model
            form.save()
            return redirect('/lab-3')
  
    context['form']= form
    return render(request, "lab3_forms.html", context)


# Create your views here.
