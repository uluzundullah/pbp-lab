from django.shortcuts import render
from .models import Note
from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers


def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

def xml(request):    
    note_xml = Note.objects.all()
    data = serializers.serialize('xml', note_xml)
    return  HttpResponse(data, content_type='application/xml')

def json(request):
    note_json = Note.objects.all()
    data = serializers.serialize('json', note_json)
    return HttpResponse(data, content_type='application/json')

# Create your views here.
