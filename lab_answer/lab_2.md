Pertanyaan:
1. Apakah perbedaan antara JSON dan XML?

2. Apakah perbedaan antara HTML dan XML?

=========================================
=========================================
Jawaban:
1. Perbedaan antara JSON dan XML sendiri bisa dilihat dibawah ini:

- JSON memiliki kepanjangan JavaScript Object Notation, XML memiliki kepanjangan Extensible Markup Language
- Untuk kompleksitas bahasa, JSON lebih mudah dibaca dibandingkan XML
- JSON hanya mendukung encoding UTF-8 sedangkan XML mendukung semua jenis encoding
- JSON mendukung array sedangkan XML tidak
- Untuk orientasi, JSON lebih ke data sedangkan XML lebih ke dokumen
- XML sendiri direkomendasikan oleh W3C (World Wide Web Consortium), sedangkan JSON pertama kali dipopulerkan oleh Douglas Crockford

2. Perbedaan XML dan HTML bisa dilihat dibawah ini:

- XML merupakan singkatan dari Extensible Markup Language, sedangkan HTML merupakan singkatan dari Hypertext Markup Language
- XML bersifat case-sensitive sedangkan HTML tidak
- XML wajib menggunakan tag penutup setelah argumen, untuk HTML sendiri bersifat opsional
- XML berfokus pada transfer data sedangkan HTML berfokus pada penyajian data
- Untuk XML, user terbebas mengunakan tag apapun sedangkan HTML membutuhkan tag-tag khusus untuk menjalankan fungsi
- XML mendukung namespace sedangkan HTML tidak

Sumber :
https://scele.cs.ui.ac.id/mod/forum/discuss.php?d=31886
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html
https://blogs.masterweb.com/perbedaan-xml-dan-html/
https://www.niagahoster.co.id/blog/xml/
https://id.strephonsays.com/json-and-vs-xml-11034

